# importing element tree
# under the alias of ET

import xml.etree.ElementTree as ET
import array
atrribute_name1 = "Amplitude_cvp"   # name of the attribute whose values are to be read i.e Amplitude_cvp, Frequency_cvp,
# input_power_dbm_cvp, output_power_dbm_cvp, gain_cvp, cross has no name given!
atrribute_name2 = "Frequency_cvp"
atrribute_name3 = "input_power_dbm_cvp"
atrribute_name4 = "output_power_dbm_cvp"
atrribute_name5 = ""
amp_values = []
freq_values=[]
input_power =[]
output_power=[]
cross_coverage=[]
output_power_modified =[]
# Passing the path of the xml document to enable the parsing process
#f= open("xmlread.txt", "w+")  # f = open(path_to_file/, mode)
tree = ET.parse('coverage_input.xml')

# getting the parent tag of the xml document
root = tree.getroot()
# printing the root (parent) tag of the xml document, along with its memory location
# print(root[2])
# printing the attributes of the first tag from the parent
#print(root[2][1][0][7][5].attrib)
#print(root[2][1][0][7][1].text)    # root[2][1][0][2].attrib is amp_values  root[2][1][0][3].attrib freq_values
# root[2][1][0][6].attrib is gain values, root[2][1][0][7].attrib is cross_coverage and root[2][1][0][7][1].text is Frequency_cvp
#print(root[2][1][0][7][5][0].text)
# f.write(root)
# printing the text contained within first subtag of the 5th tag from the parent
# print(root[5][0].text)
for child in root[2][1][0]:
    if child.get('name') == atrribute_name1:
       # print(child.attrib)
        for subchild in child:
            for subsubchild in subchild:
                amp_values.append(subsubchild.get('from'))
                # print(subsubchild.get('from'))
               # f.write(subsubchild.get('from'))
# print(amp_values)              # f.write("\n")
# f.close()
for child in root[2][1][0]:
    if child.get('name') == atrribute_name2:
       # print(child.attrib)
        for subchild in child:
            for subsubchild in subchild:
                freq_values.append(subsubchild.get('from'))
# print(freq_values)
for child in root[2][1][0]:
    if child.get('name') == atrribute_name3:
       # print(child.attrib)
        for subchild in child:
            for subsubchild in subchild:
                input_power.append(subsubchild.get('from'))
# print(input_power)
for child in root[2][1][0]:
    if child.get('name') == atrribute_name4:
       # print(child.attrib)
        for subchild in child:
            for subsubchild in subchild:
                output_power.append(subsubchild.get('from'))
                output_power.append(subsubchild.get('to'))
# print(output_power)
# replacing the output power intervals with single value
i=0
while i < len(output_power):
    output_power_modified.append(str((float(output_power[i])+ float(output_power[i+1])) / 2))
    i=i+2
print(output_power_modified, "output power")

for child in root[2][1][0]:
    if child.get('name') == atrribute_name5:
        # print(child.attrib)
        for subchild in child:
            for subsubchild in subchild:
                cross_coverage.append(subsubchild.text)
            # print(subsubchild.find("index").text)
print(cross_coverage)
# cross_coverage= list(map(lambda x:x.strip(),cross_coverage))  # to remove ´\n´ from the list

item=0
while item < (len(cross_coverage)/5):
      cross_coverage[5 * item]=freq_values[int(cross_coverage[5 * item])]
      cross_coverage[(5 * item) + 1] = amp_values[int(cross_coverage[(5 * item) + 1])]
      cross_coverage[(5 * item) + 2] = input_power[int(cross_coverage[(5 * item) + 2])]
      cross_coverage[(5 * item) + 3] = output_power_modified[int(cross_coverage[(5 * item) + 3])]
      item =item +1
print(cross_coverage)
#f.write("freq, amp and input power for ouput power 51dBW")
specific_inputs=[]
cols= []
lst = []
rows=len(output_power)
item=0
for rows in range(len(output_power_modified)): #
    item =0
    cols=[]
    lst =[]
    while item < (len(cross_coverage)/5):   # [freq, amp,input power, output power, \n]= 5
        # loop for output_power insert here
        if cross_coverage[(5 * item) + 3] ==output_power_modified[rows]:     # '51.0':
            cols.append(cross_coverage[5 * item])
            #f.write(cross_coverage[5 * item]+ "  ")
            cols.append(cross_coverage[(5 * item) + 1])
            #f.write(cross_coverage[(5 * item) + 1]+ "  ")
            cols.append(cross_coverage[(5 * item) + 2])
            #f.write(cross_coverage[(5 * item) + 2]+ "\n")
        #else :
        #   cols.append(0)
        item=item + 1
    #print(cols,"  ",len(cols))
    lst.append(output_power_modified[rows])  # lst only introduced to remove spacing between input_power entries(making it a list itself to be appended) in specific_inputs
    specific_inputs.append(lst)
    specific_inputs.append(cols)
    #print(specific_inputs, "  ", len(specific_inputs))
# f.write(specific_inputs)
#f.close()
with open('xmlread.txt','w') as f:
 L = ["[output_power] \n", "[freq, amp, input_power]...\n"]
 f.writelines(L)
 # f.write('\n'.join(specific_inputs))
 specific_inputs_entries = [' '.join(entry) for entry in specific_inputs] # gives space between elements of a list
 specific_inputs_text = '\n'.join(specific_inputs_entries)   # move to next line for new column
 f.write(specific_inputs_text)
print("freq, amp and input power for ouput power 51dBW")
print(specific_inputs)
# for i in range(len(specific_inputs)):
#    for j in range(len(cols)):
#       print(specific_inputs[i][j],end="")
# print("\n")
